const tabsTitle = document.querySelector(".tabs");
const listOfTabsTitles = document.querySelectorAll(".tabs-title");
const listContent = document.querySelectorAll("[data-title]");

tabsTitle.addEventListener("click", showContent);

function showContent(e) {
  const tabTitle = e.target.textContent.toLowerCase();

  checkAnActiveTab(e, listOfTabsTitles);
  findTextContent(tabTitle, listContent);
}

function checkAnActiveTab(e, arr) {
  arr.forEach(el => {
    if (el === e.target) {
      return e.target.classList.add("active");
    } else {
        if (el.classList.contains("active")) {
          return el.classList.remove("active");
        }
      }
    }
    )
}

function findTextContent(title, arr) {
  arr.forEach(el => {
    if (el.dataset.title === title) {
      return el.classList.add("visibilityContent");
    } else {
      if (el.classList.contains("visibilityContent")) {
        return el.classList.remove("visibilityContent");
      }
    }
  });
}

